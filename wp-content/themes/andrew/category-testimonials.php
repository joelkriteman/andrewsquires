<?php include "includes/header.php";?>
		   <div id="contentouter">   
		      <div id="content" class="testimonials">
		      
		      
		      <div id="testimonialspage">

		          <?php if (have_posts()) : ?>
		          <?php if ( is_category( 'testimonials' ) ) : 

	 query_posts( array( 'category_name' => 'testimonials', 'posts_per_page' => -1 ) ); 

endif; ?>
                <?php while (have_posts()) : the_post(); ?>
                

                <div class="testimonialitem">
                
                  <div class="testimonialimage" id="desktop">
                      <?php the_post_thumbnail('post-thumbnail', array( 'data-pin-no-hover'	=> "true")); ?>
                  </div>
                    
                  <div class="testimonialtext">
                      <?php the_content(); ?>       
                  </div>
                  
                  <div class="testimonialimage" id="tablet">
                      <?php the_post_thumbnail('extra-large-thumb', array( 'data-pin-no-hover'	=> "true")); ?>
                  </div>
                
                  <div class="clear"></div>
                
                </div>

                <hr />
               

                <?php endwhile; ?>
                <?php else : ?>

							 <?php endif; ?>
							
						</div>
						
              <div class="clear"></div>
              
              <div id="blogfooter">
                  
                  <a id="backtotop" href='#top'>Back to top</a>
                  <div id="nav-previous"><?php next_posts_link('Previous posts') ?></div>
                  <div id="pagelinks">
                   <?php
global $wp_query;

$big = 999999999; // need an unlikely integer

echo paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages
) );
?>   

</div>
            <div class="clear"></div> 
              </div>
              
		          
<?php include "includes/footer.php";?>