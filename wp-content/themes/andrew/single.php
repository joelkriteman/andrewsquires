<?php include "includes/header.php";?>
		  <div id="contentouter">    
		      <div id="content">
		      

		          <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    
                    <?php the_content(); ?>

                      
                      <div class="clear"></div>
                      
                <?php endwhile; ?>
                <?php else : ?>
							 <h2 class="center">Not Found</h2>
							 <p class="center">Sorry, but you are looking for something that isn't here.</p>
							<?php endif; ?>

						  
						  <div class="clear"></div>
						  
						  <hr>
              
              <a id="backtotop" href='#top'>Back to top</a>

		          
		          
<?php include "includes/footer.php";?>