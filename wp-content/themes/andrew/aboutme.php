<?php
/*
Template Name: About Me
*/
?>
<?php include "includes/header.php";?>
		    <div id="contentouter">
		      <div id="content" class="aboutme">
		      
		          
		            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                
                <div class="aboutblock">
                    <div class="abouttext">
                        <h4><?php the_title(); ?></h4>
                        <?php the_content(); ?>
                    </div>
                    <?php the_post_thumbnail('extra-large-thumb', array( 'data-pin-no-hover'	=> "true")); ?>
                    <div class="abouttext">
                        <?php the_field('about_top_lower'); ?>
                    </div>
                </div>
                

                
                <div class="aboutblock">
                    <img src="<?php the_field('about_image2'); ?>" data-pin-no-hover="true" alt="" />
                    <div class="abouttext">
                        <?php the_field('about_text2'); ?>
                    </div>
                </div>
                
                <div class="aboutblock">
                    <img src="<?php the_field('about_image3'); ?>" data-pin-no-hover="true" alt="" />
                    <div class="abouttext">
                        <?php the_field('about_text3'); ?>
                    </div>
                </div>
                
                <div class="aboutblock">
                    <img src="<?php the_field('about_image4'); ?>" data-pin-no-hover="true" alt="" />
                    <div class="abouttext">
                        <?php the_field('about_text4'); ?>
                    </div>
                </div>
                
                <div class="aboutblock">
                    <div class="abouttext">
                        <?php the_field('about_text5'); ?>
                    </div>
                </div>
                          
                
              <?php endwhile; ?>
                <?php else : ?>
							 <h2 class="center">Not Found</h2>
							 <p class="center">Sorry, but you are looking for something that isn't here.</p>
							<?php endif; ?>                        


		      
		      

		          
		          <div class="clear"></div>
		          
		          <hr />
              
              <a id="backtotop" href='#top'>Back to top</a>         
		          		          
		          
<?php include "includes/footer.php";?>