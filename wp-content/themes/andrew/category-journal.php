<?php include "includes/header.php";?>
		   <div id="contentouter">   
		      <div id="content" class="journalcat">
		      
		      <?php include "includes/journalwidgets.php";?>
		      
		      <div id="journalcatpage">

		          <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                
                <div class="journalblogitem">
                
                    <div class="journalcatimage">
                        <a href="<?php the_permalink(); ?>">
                        		<?php the_post_thumbnail('large-thumb', array( 'data-pin-no-hover'	=> "true")); ?>
                        </a>
                    </div>
                
                    <div class="journalintro">
                        <div class="journalintrotext">
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            <?php the_excerpt(); ?>
                            <a class="continuereading" href="<?php the_permalink(); ?>">Continue reading...</a>
                        </div>
                        <div class="journaldate"><?php the_time('F jS, Y') ?></div>               
                    </div>
                
                <div class="clear"></div>
                
                </div>
                          
                
                
                <hr />
               

                <?php endwhile; ?>
                <?php else : ?>

							 <?php endif; ?>
							
						</div>
						
              <div class="clear"></div>
              
              <div id="blogfooter">
                  
                  <a id="backtotop" href='#top'>Back to top</a>
                  <div id="nav-previous"><?php next_posts_link('Previous posts') ?></div>
                  <div id="pagelinks">
                   <?php
global $wp_query;

$big = 999999999; // need an unlikely integer

echo paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages
) );
?>   

</div>
             <div class="clear"></div>
              </div>
              
		          
<?php include "includes/footer.php";?>