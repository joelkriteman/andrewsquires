		    </div>
		      </div>
        <!-- end content -->
		      
		    <div class="clear"></div>

        <!-- begin footer -->		      
        <div id="footer">
        
          <!-- begin footerinner -->
          <div id="footerinner">
		            
		          <!-- begin footerright -->	
		          <div id="footerright">
		              <ul>
		                  <li id="facebook">
		                      <a href="https://www.facebook.com/pages/Andrew-J-R-Squires-Photography/208699899152666" rel="external" >
		                          <img src="<?php bloginfo('template_url'); ?>/images/blank.gif" alt="" />
		                      </a>
		                  </li>
		                  <li id="twitter">
		                      <a href="https://twitter.com/andysquires" rel="external" >
		                          <img src="<?php bloginfo('template_url'); ?>/images/blank.gif" alt="" />
		                      </a>
		                  </li>
		                  <li id="pinterest">
		                      <a href="http://www.pinterest.com/andrewjrsquires/" rel="external">
		                          <img src="<?php bloginfo('template_url'); ?>/images/blank.gif" alt="" />
		                      </a>
		                  </li>
		                  <li id="instagram">
		                      <a href="http://instagram.com/andysquires" rel="external">
		                          <img src="<?php bloginfo('template_url'); ?>/images/blank.gif" alt="" />
		                      </a>
		                  </li> 			
		              </ul>
		                
		            </div>
		            <!-- end footerright -->
		            
		            <!-- begin footerleft -->
		            <div id="footerleft">
		                <p><a href="http://www.everybodysmile.biz/andrewjrsquiresphotographyltd_1291/clien/" rel="external">Client login</a></p>
		            </div>
		            <!-- end footerleft -->

		        </div>
		        <!-- end footerinner -->
		            
		      </div>
          <!-- end footer -->
	
          <!-- begin footer two -->	     
		      <div id="footertwo">
		      
		          <div id="footertwoinner">
		          
		            <div id="footertworight">
		              Design: <a href="http://www.lewieevans.co.uk" rel="external">Lewie Evans</a>
		            </div>
		            
		            <div id="footertwoleft">
		              <p>&copy; Copyright Andrew J.R. Squires <?php print date('Y') ?></p>
		            </div>
		          
		          </div>
		      
		      </div>
          <!-- end footer two -->			 
               
		      <div id="preload">
		        <img src="<?php bloginfo('template_directory'); ?>/images/facebook_over.png" alt="" />
		        <img src="<?php bloginfo('template_directory'); ?>/images/twitter_over.png" alt="" />
		        <img src="<?php bloginfo('template_directory'); ?>/images/pinterest_hover.png" alt="" />
		        <img src="<?php bloginfo('template_directory'); ?>/images/instagram_hover.png" alt="" />
		      </div>
		          
		   </div>       
		  </div>
		  
		  
  <script src="<?php bloginfo('template_url'); ?>/js/jquery-1.9.1.js" type="text/javascript"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/externallinks.js" type="text/javascript"></script>

  <script type="text/javascript">
$(document).ready(function() {
  	  $('body').addClass('js');
		  var $menu = $('#menu'),
		  	  $menulink = $('.menu-link'),
		  	  $menuTrigger = $('#menu-item-1189 > a');

		$menulink.click(function(e) {
			e.preventDefault();
			$menulink.toggleClass('active');
			$menu.toggleClass('active');
		});

		$menuTrigger.click(function(e) {
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass('active').next('ul').toggleClass('active');
		});

		});
//@ sourceURL=pen.js
  </script>

		<?php wp_footer(); ?>  
		</body>

</html>