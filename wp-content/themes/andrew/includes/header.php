<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'
      'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
 <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<meta name="Description" content="Andrew JR Squires Wedding Photography Brighton and Hove East Sussex." />
	<meta name="keywords" content="Andrew Squires" />
	<meta name="url" content="http://www.andrewjrsquires.com" />
	<meta name="rating" content="Safe For Kids" />
	<meta name="revisit-after" content="30 days" />
	<meta name="robots" content="all" />
	<meta name="copyright" content="Copyright &copy;Andrew Squires" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Andrew JR Squires - Creative Wedding Engagement Family Photography</title>
	
	<link href="<?php bloginfo( 'stylesheet_url' ); ?>" rel="stylesheet" type="text/css"  media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/navigation.css" type="text/css" media="screen" />
	<?php wp_head(); ?>
	<script type="text/javascript" src="//use.typekit.net/iub3lbu.js"></script>
  <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-22397733-1', 'andrewjrsquires.com');
  ga('send', 'pageview');

</script>
</head>
    
		<body>
		  <a name="top"></a>
		  <div id="wrapper">
		    <div id="page">

        <!-- begin header -->
		    <div id="header">
		      
		      <!-- begin header content -->	
		      <div id="headercontent">


              <!-- begin header right -->		        
		          <div id="headerright">
		              <ul>
		                  <li id="facehead">
		                      <a href="https://www.facebook.com/pages/Andrew-J-R-Squires-Photography/208699899152666" rel="external" >
		                          <img src="<?php bloginfo('template_url'); ?>/images/blank.gif" alt="" />
		                      </a>
		                  </li>
		                  <li id="twitterhead">
		                      <a href="https://twitter.com/andysquires" rel="external" >
		                          <img src="<?php bloginfo('template_url'); ?>/images/blank.gif" alt="" />
		                      </a>
		                  </li>
		                  <li id="pinteresthead">
		                      <a href="http://www.pinterest.com/andrewjrsquires/" rel="external">
		                          <img src="<?php bloginfo('template_url'); ?>/images/blank.gif" alt="" />
		                      </a>
		                  </li>
		                  <li id="instagramhead">
		                      <a href="http://instagram.com/andysquires" rel="external">
		                          <img src="<?php bloginfo('template_url'); ?>/images/blank.gif" alt="" />
		                      </a>
		                  </li> 			
		              </ul>	               
		          </div>
              <!-- end header right -->
		        
                <!-- begin header left -->	
		            <div id="cameralogo">
		              <a href="<?php bloginfo('home'); ?>">
		                  <img src="<?php bloginfo('template_url'); ?>/images/logo1.png"alt="" />
		              </a>
		            </div>
                <!-- end header left -->
		            

                <!-- begin header centre -->
		            <p id="logo">
		                <a href="<?php bloginfo('home'); ?>">
		                  <img src="<?php bloginfo('template_url'); ?>/images/logo2.png"alt="Andrew Squires logo" />
		                </a>
		            </p>
                <!-- end header centre -->
		        </div>
		        <!-- end header content -->	
		        
		            <div id="menucontent">
              		<a href="#menu" class="menu-link"><img src="<?php bloginfo('template_url'); ?>/images/menu.png" /></a>
		                <div id="menu" class="squish">
		                    <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
		                </div>
		            </div>

		        
		      </div>
		      <!-- end header -->
		      
		      <div class="clear"></div>