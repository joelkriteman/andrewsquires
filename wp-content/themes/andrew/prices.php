<?php
/*
Template Name: Prices
*/
?>
<?php include "includes/header.php";?>
		  <div id="contentouter">    
		      <div id="content" class="prices">
		      
		          
		            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                

                    <div class="pricestext" id="desktop">
                        <h4><?php the_title(); ?></h4>
                        <?php the_content(); ?>
                    </div>
                  
                    <?php the_post_thumbnail('medium-thumb', array( 'data-pin-no-hover'	=> "true")); ?>
                    
                    <div class="pricestext" id="mobile">
                        <h4><?php the_title(); ?></h4>
                        <?php the_content(); ?>
                    </div>
                    
                    <img id="pricesimage2" src="<?php the_field('prices_second_image'); ?>" data-pin-no-hover="true" alt="" />
                    
                    <div class="pricestext" id="tablet">
                        <h4><?php the_title(); ?></h4>
                        <?php the_content(); ?>
                    </div>
                    
                    <img id="pricesimage3" src="<?php the_field('prices_third_image'); ?>" data-pin-no-hover="true" alt="" />
                    


                    
                    
                      
                
              <?php endwhile; ?>
                <?php else : ?>
							 <h2 class="center">Not Found</h2>
							 <p class="center">Sorry, but you are looking for something that isn't here.</p>
							<?php endif; ?>
							

		      
		      <div class="clear"></div>	
		      

		          
		                    
		          		          
		          
<?php include "includes/footer.php";?>