<?php
/*
Template Name: Contact
*/
?>
<?php include "includes/header.php";?>
		   <div id="contentouter">   
		      <div id="content" class="contact">
		      
		          
		            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                
                <div id="contactleft">
                    <div id="contacttext">
                        <h4><?php the_title(); ?></h4>
                        <?php the_content(); ?>
                    </div>
                    <?php the_post_thumbnail('post-thumbnail', array( 'data-pin-no-hover'	=> "true")); ?>
                </div>    
                
              <?php endwhile; ?>
                <?php else : ?>
							 <h2 class="center">Not Found</h2>
							 <p class="center">Sorry, but you are looking for something that isn't here.</p>
							<?php endif; ?>
							
							<div id="contactform">
							   <?php the_field('contact_form'); ?>
							</div>
		      
		      <div class="clear"></div>	
		      
		          
		                    
		          		          
		          
<?php include "includes/footer.php";?>