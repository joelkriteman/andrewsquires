<?php include "includes/header.php";?>
		   <div id="contentouter">   
		      <div id="content" class="weddingscat">
		        <ul>
		          <?php if  (have_posts()) : ?>
		          
		          <?php if ( is_category( 'weddings' ) ) : 

	 query_posts( array( 'category_name' => 'weddings', 'posts_per_page' => -1 ) ); 

endif; ?>
		        
                <?php while (have_posts()) : the_post(); ?>
                             
                
                
                    <li>
                        <a href="<?php the_permalink(); ?>">
                         <?php the_post_thumbnail('post-thumbnail', array( 'data-pin-no-hover'	=> "true")); ?>
                          <h4><?php the_title(); ?></h4>
                        </a>
                    </li>
                
                

                <?php endwhile; ?>
                <?php else : ?>

							 <?php endif; ?>
							
						  </ul>
						
              <div class="clear"></div>
              
              <hr />
              
              <a id="backtotop" href='#top'>Back to top</a>
		          
		          
<?php include "includes/footer.php";?>