<?php include "includes/header.php";?>
		   <div id="contentouter">   
		      <div id="content" class="press">
		      
		      
		      <div id="presspage">

		          <?php if (have_posts()) : ?>
		          <?php if ( is_category( 'press' ) ) : 

	 query_posts( array( 'category_name' => 'press', 'posts_per_page' => -1 ) ); 

endif; ?>
                <?php while (have_posts()) : the_post(); ?>
                
                <div class="pressitem">
                
                    <div class="presstext">
                        <?php the_content(); ?>
                            <div class="clear"></div>
                            <div class="pressdate">
                                <?php the_time('F jS, Y') ?>
                            </div> 

                    </div>
                    <?php the_post_thumbnail('extra-large-thumb', array( 'data-pin-no-hover'	=> "true")); ?>

                </div>

                <div class="clear"></div>
                
                <hr />
               

                <?php endwhile; ?>
                <?php else : ?>

							 <?php endif; ?>
							
						</div>
						
              <div class="clear"></div>
              
              <div id="blogfooter">
                  
                  <a id="backtotop" href='#top'>Back to top</a>
                  <div id="nav-previous"><?php next_posts_link('Previous posts') ?></div>
                  <div id="pagelinks">
                   <?php
global $wp_query;

$big = 999999999; // need an unlikely integer

echo paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages
) );
?>   

</div>
            <div class="clear"></div> 
              </div>
              
		          
<?php include "includes/footer.php";?>