<?php
/*
Template Name Posts: Journal
*/
?>

<?php include "includes/header.php";?>
		   <div id="contentouter">   
		      <div id="content" class="journal">
		      
		        <?php include "includes/journalwidgets.php";?>

		          <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                
               <div class="journalsingleitem">
                
                    <div class="journalsingleintro">
                            <h4><?php the_title(); ?></h4>
                            <?php the_content(); ?>
                        <?php the_time('F jS, Y') ?>            
                    </div>
                
                    <div class="clear"></div>
                
                    <div class="bodyimage">
                        <?php the_post_thumbnail('extra-large-thumb'); ?>
                    </div>
                    
                    
                    <?php if( get_field('small_image_upper_left') ): ?>
                    <div class="bodyimage">
                          <img class="leftimage" src="<?php the_field('small_image_upper_left'); ?>" alt="" />
                            <?php if( get_field('small_image_upper_right') ): ?>
                              <img class="rightimage" src="<?php the_field('small_image_upper_right'); ?>" alt="" />
                            <?php endif; ?>
                            <?php if( get_field('small_image_lower_left') ): ?>
                              <img class="leftimage" src="<?php the_field('small_image_lower_left'); ?>" alt="" />
                            <?php endif; ?>
                            <?php if( get_field('small_image_lower_right') ): ?>
                              <img class="rightimage" src="<?php the_field('small_image_lower_right'); ?>" alt="" />
                            <?php endif; ?>
                    </div>
                    <?php endif; ?>
                    
                    <div class="clear"></div>
                    
                    
                    <?php if( get_field('gallery') ): ?>
                        <?php the_field('gallery'); ?> 
                    <?php endif; ?>
                    
                    
                    <?php if( get_field('middle_text') ): ?>
                    <div class="journalsingleintro subtext">
                        <?php the_field('middle_text'); ?>
                    </div>
                    <?php endif; ?>
                    
                    
                    <?php if( get_field('gallery_2') ): ?>
                        <?php the_field('gallery_2'); ?> 
                    <?php endif; ?>
                    
                    
                    <?php if( get_field('lower_text') ): ?>
                      <div class="journalsingleintro subtext">
                             <?php the_field('lower_text'); ?>   
                      </div>
                    <?php endif; ?>
                    
                    <?php if( get_field('gallery_3') ): ?>
                        <?php the_field('gallery_3'); ?> 
                    <?php endif; ?>
                    
                    
                    <?php if( get_field('even_lower_text') ): ?>
                      <div class="journalsingleintro subtext">
                             <?php the_field('even_lower_text'); ?>   
                      </div>
                    <?php endif; ?>
                    
                    <?php if( get_field('gallery_4') ): ?>
                        <?php the_field('gallery_4'); ?> 
                    <?php endif; ?>
                    
                    
                    <?php if( get_field('last_text') ): ?>
                      <div class="journalsingleintro subtext">
                             <?php the_field('last_text'); ?>   
                      </div>
                    <?php endif; ?>
                    
                                        
                     
                    
                    
                    <div id="socialshare">
                        <div id="sharethelove">Share the love</div>
                        <div id="shareicons">
                          <?php echo do_shortcode("[social_share/]"); ?>
                        </div>   
                    </div>
                    
                    <div class="clear"></div>
                
                <hr />       
                
                </div>
                      
                <?php endwhile; ?>
                <?php else : ?>
							 <h2 class="center">Not Found</h2>
							 <p class="center">Sorry, but you are looking for something that isn't here.</p>
							<?php endif; ?>

						  
						  <div class="clear"></div>
						  
						  
						  <div id="relatedposts">
						  
						  <h4>Related Posts</h4>
						  
						  
						   <?php
	global $post;
	$categories = get_the_category();
	$thiscat = $categories[0]->cat_ID;
?>
<?php query_posts('showposts=3&orderby=rand&cat=journal' . $thiscat); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<ul>
		<li>
		  <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		  <?php the_excerpt(); ?>
		  <div class="relateddate"><?php the_time('F jS, Y') ?></div>   
		</li>
</ul>							

	<?php endwhile; else : ?>
		Error returning query.
	<?php endif; ?>
<?php wp_reset_query(); ?>						  
						  </div>
						  
						  
						  
						  <div id="commentspanel">
										 <?php comments_template(); ?>
										 <div class="clear"></div>						
						  </div>
						  
						  <div class="clear"></div>
						  
						  <hr />
						  
						  <div id="blogfooter">
              
                  <a id="backtotop" href='#top'>Back to top</a>
                  <div id="nav-previous"><a href="<?php bloginfo('home'); ?>/category/journal">Return to journal entries</a></div>
                  <div class="clear"></div>
              </div>

		          
		          
<?php include "includes/footer.php";?>