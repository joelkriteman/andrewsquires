<?php
/*
Template Name: Home
*/
?>
<?php include "includes/header.php";?>
	<div id="contentouter">
	     <!-- begin content -->	      
		      <div id="content">
		      
		        
		          <div id="homeslider">
		              <div class="homeimage">
		                  <?php if ( function_exists( 'soliloquy_slider' ) ) soliloquy_slider( 'home-welcome' ); ?>
		              </div>
		          </div>
		          
		          
		          <div id="homeweddings">
		              <div class="homeimage" id="homeimagetop">
		                 <a href="<?php bloginfo('home'); ?>/category/weddings">
		                    <img src="<?php the_field('home_weddings_image'); ?>" data-pin-no-hover="true" alt="" />
		                    <h4>weddings</h4>
		                 </a>
		              </div>
		              <div class="homeimage">
		                <a href="<?php bloginfo('home'); ?>/your-experience">
		                  <img src="<?php the_field('home_about_day_image'); ?>" data-pin-no-hover="true" alt="" />
		                  <h4>About Your Day</h4>
		                </a>
		              </div>
		          </div>
		          
		          <div class="clear"></div>
		          
		          <div id="homeabout">
		            <div class="homeimage" id="homeaboutfirst">
		              <a href="<?php bloginfo('home'); ?>/about-me">
		                  <img src="<?php the_field('home_about_me_image'); ?>" data-pin-no-hover="true" alt="" />
		                  <h4>A Little About Me</h4>
		              </a>
		              </div>
		              <div class="homeimage">
		                <a href="<?php bloginfo('home'); ?>/contact-me">
		                  <img src="<?php the_field('home_get_in_touch_image'); ?>" data-pin-no-hover="true" alt="" />
		                  <h4>Get in Touch</h4>
		                </a>
		              </div>
		              <div class="homeimage" id="homelast">
		                <a href="<?php bloginfo('home'); ?>/category/journal">
		                  <img src="<?php the_field('home_journal_image'); ?>" data-pin-no-hover="true" alt="" />
		                  <h4>Latest on the Journal</h4>
		                </a>
		              </div> 
		          </div>
		          
		          <div class="clear"></div>	          
		          		          
		          
<?php include "includes/footer.php";?>