<?php
/*
Template Name: Your Experience
*/
?>
<?php include "includes/header.php";?>
		  <div id="contentouter">    
		      <div id="content" class="experience">
		      
		          
		            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                

                    <div class="experiencetext" id="desktop">
                        <h4><?php the_title(); ?></h4>
                        <?php the_content(); ?>
                    </div>
                  
                    <?php the_post_thumbnail('medium-thumb', array( 'data-pin-no-hover'	=> "true")); ?>
                    <div id="wide"><img id="experienceimage2" src="<?php the_field('experience_second_image'); ?>" data-pin-no-hover="true" alt="" /></div>
                    
                    
                    <div class="experiencetext" id="tablet">
                        <h4><?php the_title(); ?></h4>
                        <?php the_content(); ?>
                    </div>
                     
                    <div id="mobile"><img id="experienceimage2" src="<?php the_field('experience_second_image'); ?>" data-pin-no-hover="true" alt="" /></div> 
                
              <?php endwhile; ?>
                <?php else : ?>
							 <h2 class="center">Not Found</h2>
							 <p class="center">Sorry, but you are looking for something that isn't here.</p>
							<?php endif; ?>
							

		      
		      <div class="clear"></div>	
		      

		          
		                    
		          		          
		          
<?php include "includes/footer.php";?>