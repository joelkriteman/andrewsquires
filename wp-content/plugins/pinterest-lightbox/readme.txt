=== Pinterest Lightbox ===
Contributors: travis.hoglund
Donate link: http://travishoglund.com/donate/
Tags: pinterest, pinterest lightbox, lightbox, pinterest pop up, pinterest share, pinterest plugin, wordpress pinterest, wp pinterest, pinterest images, pin images, nextgen pinterest, nextgen pin
Requires at least: 2.8.0
Tested up to: 3.3.1
Stable tag: 1.2

Add Pinterest to the NextGEN Gallery plugin.

== Description ==

With over 5 million downloads the NextGEN Gallery plugin is obviously widely used. The Pinterest Lightbox plugin enables a PinIt Button on each image automatically through your NextGEN Gallery.  It automatically uses the caption, image URL, and page URL from NextGEN to sync with the Pinterest API.  This plugin is very useful for photo-intensive websites. 

To use it, simply download, and activate.  To prevent confliction with the default Lightbox included in NextGEN navigate to Gallery -> Options -> Effects Tab, and from the dropdown "JavaScript Thumbnail effect" select None.  Everything should be working great!  Check the screenshots for any confusion. 


== Installation ==

Install this plugin in the normal way:

1. Place it in your `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

To use it, simply download, and activate.  To prevent confliction with the default Lightbox included in NextGEN navigate to Gallery -> Options -> Effects Tab, and from the dropdown "JavaScript Thumbnail effect" select None.  Everything should be working great!  Check the screenshots for any confusion. 


== Frequently Asked Questions ==


= Do I need to input any of my Pinterest info? =

No.  This plugin is just promoting your website photos, so it has nothing to do with YOUR Pinterest account.

= Is there any worry of confliction with other plugins? =

This plugin is based off of Slimbox, but all of the functions have been renamed so there will be no harsh confliction.  However, most Lightbox plugins do not get along well, so it is probably a good idea to only have one active.

== Screenshots ==

1. Example of an image popup with the PinIt Button enabled.
2. Changing options in NextGEN Gallery to prevent confliction.

== Changelog ==

= 1.0 =
* Version 1.0 Released.

= 1.1 =
* Version 1.1 Released.
* Fixes Bug where overlay was on top of photo in WP 3.6

== Upgrade Notice ==

= 1.0 =
Future versions to come.

