<?php

class C_NextGen_Pro_Lightbox_Installer
{
	function get_registry()
	{
		return C_Component_Registry::get_instance();
	}

    function set_attr(&$obj, $key, $val)
    {
        if (!isset($obj->$key))
            $obj->$key = $val;
    }

	function install($reset=FALSE)
	{
        $router = $this->get_registry()->get_utility('I_Router');

		// Install or update the lightbox library
		$mapper = $this->get_registry()->get_utility('I_Lightbox_Library_Mapper');
		$lightbox = $mapper->find_by_name(NGG_PRO_LIGHTBOX);
		if (!$lightbox)
            $lightbox = new stdClass;

        // Set properties
        $lightbox->name	= NGG_PRO_LIGHTBOX;
		$this->set_attr($lightbox, 'title', "NextGEN Pro Lightbox");
		$this->set_attr($lightbox, 'code', "class='nextgen_pro_lightbox' data-nplmodal-gallery-id='%PRO_LIGHTBOX_GALLERY_ID%'");
        $this->set_attr(
            $lightbox,
            'css_stylesheets',
            implode("\n", array(
                'photocrati-nextgen_pro_lightbox#style.css'
            ))
        );
        $this->set_attr(
            $lightbox,
            'styles',
            implode("\n", array(
                'photocrati-nextgen_pro_lightbox#style.css'
            ))
        );
		$this->set_attr(
            $lightbox,
            'scripts',
            implode("\n", array(
                'photocrati-nextgen_pro_lightbox#jquery.mobile_browsers.js',
                "photocrati-nextgen_pro_lightbox#nextgen_pro_lightbox.js"
            ))
        );
        $this->set_attr(
            $lightbox,
            'display_settings',
            array(
                'icon_color'  => '',
                'carousel_text_color' => '',
                'background_color' => '',
                'carousel_background_color' => '',
                'sidebar_background_color' => '',
                'router_slug' => 'gallery',
                'transition_effect' => 'slide',
                'enable_routing' => '1',
                'enable_comments' => '1',
                'enable_sharing' => '1',
                'display_comments' => '0',
                'display_captions' => '0',
                'display_carousel' => '1',
                'transition_speed' => '0.4',
                'slideshow_speed' => '5',
                'style' => '',
                'touch_transition_effect' => 'slide',
                'image_pan' => '0',
                'interaction_pause' => '1',
                'image_crop' => '0'
            )
        );

		$mapper->save($lightbox);
	}

    function uninstall_nextgen_pro_lightbox($hard = FALSE)
    {
        $mapper = $this->get_registry()->get_utility('I_Lightbox_Library_Mapper');
        if (($lightbox = $mapper->find_by_name(NGG_PRO_LIGHTBOX)))
            $mapper->destroy($lightbox);
    }
}
